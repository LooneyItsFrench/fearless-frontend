function createCard(name, description, pictureUrl, dateStart, dateEnd, locationName) {
    return (`
    <div class = 'col'>
    <div class="card-shadow p1 m-1 mb-1 bg-body rounded"></div>
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h7 class="card-subtitle mb-2 text-muted">${locationName}</h7>
          <p class="card-text">${description}</p>
          <div class="card-footer">${dateStart} - ${dateEnd}</div>
        </div>
      </div>
    </div>
    `);
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try{
        const response = await fetch(url);

        if (!response.ok) {
            console.log("Bad response");
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    console.log(details);
                    const dateObjectStart = new Date(details.conference.starts);
                    const dateObjectEnd = new Date(details.conference.ends);
                    const dateStart = dateObjectStart.getMonth() + "/" + dateObjectStart.getDate() + "/" + dateObjectStart.getFullYear();
                    const dateEnd = dateObjectEnd.getMonth() + "/" + dateObjectEnd.getDate() + "/" + dateObjectEnd.getFullYear();
                    const locationName = details.conference.location.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const html = createCard(title, description, pictureUrl, dateStart, dateEnd, locationName);
                    const column = document.querySelector('.row'); 
                    column.innerHTML += html;
                    
                }
                
            }  
        }
    } catch (e) {
    }
});
