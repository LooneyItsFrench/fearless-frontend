window.addEventListener('DOMContentLoaded', async () => {
    

    const url = 'http://localhost:8000/api/states/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById('state');
        console.log(data.states);

        for (let state of data.states) {
            const option = document.createElement('option');
            console.log(option);
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            selectTag.appendChild(option);
        }
    }

});